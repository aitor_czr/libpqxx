#ifndef CONNECT_SQL_H
#define CONNECT_SQL_H

class connect_Sql{
 private:
  string dbname;
  string user;
  string password;
  string hostaddr;
  string port;
  static connect_Sql* pInstance;
  static void destroy_connect_Sql();

 protected:
  connect_Sql(char * cad1, char * cad2, char * cad3, char * cad4, char * cad5);
  connect_Sql(const connect_Sql &s){};                //Desaprobada ??
  connect_Sql &operator = (const connect_Sql & ){};   //Desaprobada ??
 
 public:
  virtual ~connect_Sql();
  static connect_Sql& getInstance(char *, char *, char *, char *, char *);  
  int select_int_from_where (string elements, string table, string el, string value);
};

#endif //CONNECT_SQL_H