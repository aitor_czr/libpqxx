#include "../include/headers.h"

connect_Sql* connect_Sql::pInstance = NULL;

connect_Sql::connect_Sql(char * cad1, char * cad2, char * cad3, char * cad4, char * cad5)
{
 this -> dbname = string(cad1);
 this -> user = string(cad2);
 this -> password = string(cad3);
 this -> hostaddr = string(cad4);
 this -> port = string(cad5);
}

connect_Sql::~connect_Sql()
{
 free(pInstance);
}

connect_Sql& connect_Sql::getInstance(char * dbname, char * user, char * password, char * hostaddr, char * port)
{
 if(pInstance == NULL)
                      {
		       pInstance = new connect_Sql(dbname, user, password, hostaddr, port);    //Invoca a connect_Sql(...)
		       atexit(& destroy_connect_Sql);    // Al salir, destruimos el objeto Singleton
                      }
 return *pInstance;
}

void connect_Sql::destroy_connect_Sql()
{
 if(pInstance != NULL) delete pInstance; //Invoca a ~connect_Sql()
}

int connect_Sql::select_int_from_where (string elements, string table, string el, string value)
{
  string conn = "dbname = " + dbname + " user = " + user + " password = " + password \
               + " hostaddr = " + hostaddr + " port = " + port;

 try{ 
      connection C(conn);
      if (C.is_open()) {
                        // cout << "La conexión a la base de datos: " << C.dbname() << " se ha realizado con éxito. " << endl;
                       }
         else {
               cout << "Error de conexión a la base de datos !!" << endl;
               return (-1);
              }
       
       string S = string("SELECT ") + elements + string(" FROM ") \
                + table + string(" WHERE ") + el + " = " + value; 
     
       const char * sql = S.c_str();
    
       // Crear un objeto no transaccional:
       nontransaction N(C);

       // Executar SQL query
       result R( N.exec( sql ));
       C.disconnect ();
       result::const_iterator a = R.begin();
       return a[0].as<int>();

    }catch (const std::exception &e)
           {
            cerr << e.what() << std::endl;
            return (-1);
           }
}